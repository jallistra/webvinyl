"""
In dieser Datei wird die webvinyl-App definiert.
"""

from django.apps import AppConfig

class WebvinylConfig(AppConfig):
    name = 'webvinyl'
