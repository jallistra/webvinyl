"""
In dieser Datei werden die Model-Klassen der webvinyl-App angegeben, 
deren zugeordnete Datenbank-Tabellen über das Admin-Menü verwaltet werden können sollen.
"""

from django.contrib import admin
from .models import Post, Comment, Album

# Register your models here.
admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Album)
