"""web_project URL Configuration

Dies ist die übergeordnete URL-Konfiguration des Projekts. 
Die Liste wird von oben nach unten durchgegangen bis der erste Pattern-Match erfolgt.
"""

from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('webvinyl.urls')),
    path('', include('users.urls')),
]

