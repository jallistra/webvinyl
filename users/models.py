""" In dieser Datei werden die Klassen definiert, aus denen die Datenbank-Tabellen für die "users"-App generiert werden """

from django.db import models
from django.contrib.auth.models import User
from PIL import Image

User._meta.get_field('email')._unique = True

#Die Profile-Klasse fungiert als Erweiterung der integrierten User-Klasse von Django. Sie enthält zusätzliche Attribute 
# für Profilbilder, Beschreibung, Follower
# follows ermöglicht beliebig vielen Nutzern beliebig vielen anderen Nutzern zu folgen
# 'Followen' ist dabei im Gegensatz zu einem Freunde-System asymmetrisch, d.h. man muss seinen Followern nicht selbst folgen 
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    follows = models.ManyToManyField('self', related_name='followed_by', blank=True, symmetrical=False)
    description = models.TextField(blank=True)


    def __str__(self):
        return f'{self.user.username} Profil'

    def save(self, *args, **kwargs):
        super().save()

        img = Image.open(self.image.path)
        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)