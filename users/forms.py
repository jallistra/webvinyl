"""
In dieser Datei werden Formulare/Eingabefelder der users-App definiert, die auf der Website verwendet werden.
"""

from django import forms
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User
from .models import Profile
from django.contrib.auth.forms import UserCreationForm


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField(label="E-Mail Adresse")

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField(label="E-Mail Adresse")

    class Meta:
        model = User
        fields = ['username', 'email']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['description', 'image']
        labels = {
            'description': _('Beschreibung'),
        }

