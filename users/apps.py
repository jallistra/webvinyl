"""
In dieser Datei wird die users-App definiert.
"""

from django.apps import AppConfig

class UsersConfig(AppConfig):
    name = 'users'

# Dieser Funktion wird benötigt, damit bei der Registrierung eines Nutzers automatisch ein Profil angelegt wird.
    def ready(self):
        import users.signals