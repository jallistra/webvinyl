import spotipy
import spotipy.oauth2 as oauth2

def get_spotify_uri(albumstr, artiststr):
        credentials = oauth2.SpotifyClientCredentials(
                client_id='b7f6213e73b64eeab514d2f7f015c78a',
                client_secret='2578f6c001b144d4803c24ae61993f4b')

        token = credentials.get_access_token()
        spotify = spotipy.Spotify(auth=token)

        query = 'album:'+albumstr+' artist:'+artiststr

        result = spotify.search(query, limit=1, offset=0, type='album', market=None)
        albums = result['albums']['items']
        album = albums[0]
        uri = album['uri'][14:]

        return uri

        # embed_code = <iframe src="https://open.spotify.com/embed/album/'+uri+'" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


