"""
In dieser Datei werden Formulare/Eingabefelder der webvinyl-App definiert, die auf der Website verwendet werden.
"""

from django import forms
from django.utils.translation import gettext_lazy as _
from .models import Post, Comment

class MusicSearchForm(forms.Form):
    MusicSearchWord = forms.CharField(label='Suchwort', max_length=128)
    key_norm = forms.HiddenInput()
class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['content']
        labels = {
            'content': _('Post schreiben:'),
        }
class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['content']
        labels = {
            'content': _('Kommentar schreiben:'),
        }        

class AdvancedSearchForm(forms.Form):
    artist = forms.CharField(required=False, max_length=100, label='Künstler')
    name = forms.CharField(required=False, max_length=100, label='Titel')
    number = forms.CharField(required=False, max_length=100, label='Katalognummer')
    label = forms.CharField(required=False, max_length=100, label='Label')
    key_adv = forms.HiddenInput()

class UserSearchForm(forms.Form): 
    username = forms.CharField(label='Your name', max_length=64)
