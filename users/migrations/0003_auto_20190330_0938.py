# Generated by Django 2.1.7 on 2019-03-30 09:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_profil_follows'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profil',
            name='follows',
            field=models.ManyToManyField(blank=True, related_name='followed_by', to='users.Profil'),
        ),
    ]
