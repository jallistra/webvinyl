"""
Hier können Methoden angelegt werden, die ausgeführt werden, 
wenn ein Datenbank- bzw. Modell-Objekt kreiert oder aktualisiert wird
"""

from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from .models import Profile

# Diese Methoden sorgen dafür, dass beim Erstellen eines neuen Nutzers auch ein zugehöriges Profil erstellt wird
@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    instance.profile.save()