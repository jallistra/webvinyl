"""
In dieser Datei werden die Logiken definiert, die beim Aufruf einer URL der webvinyl-App ausgeführt werden.
"""

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.views.generic import (
    UpdateView,
    DeleteView
)
from .forms import UserSearchForm, MusicSearchForm, PostForm, CommentForm, AdvancedSearchForm
from users.forms import UserUpdateForm, ProfileUpdateForm
from django.shortcuts import redirect
from users.models import User 
from django.contrib.auth.decorators import login_required
from utils.discogs_search import basic_search, vinyl_search, advanced_search
from utils.spotify_search import get_spotify_uri
from .models import Post, Album, Comment
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib import messages

@login_required
def base(request):
    user = request.user
    all_follows = user.profile.follows.all()
    own_posts = Post.objects.filter(author=user.profile)
    followee_posts = Post.objects.filter(author__in=all_follows)
    all_posts = own_posts | followee_posts
    posts = all_posts.distinct().order_by('-date_posted')

    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user.profile
            post.published_date = timezone.now()
            post.save()
            return redirect('base')
    else:
        form = PostForm()
    return render(request, 'webvinyl/news.html', {'form': form, 'posts':posts})

@login_required
def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    comments = Comment.objects.filter(post=post).order_by('date_posted')

    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user.profile
            comment.published_date = timezone.now()
            comment.post = post
            comment.save()
            return redirect('post-detail', pk=post.pk)
    else:
        form = CommentForm()  
    return render(request, 'webvinyl/post_detail.html', {'form': form, 'post':post, 'comments': comments})      



class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['content']

    def form_valid(self, form):
        form.instance.author = self.request.user.profil
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user.profile == post.author:
            return True
        return False    

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post   
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user.profile == post.author:
            return True
        return False         
   
@login_required
def search_user(request):
    if request.method == "POST":
        form = UserSearchForm(request.POST)
        if form.is_valid():            
            return redirect('found_user', request.POST.get("username", ""))
    else:
        form = UserSearchForm()    
    return render(request, 'webvinyl/search_user.html', {'form': form})    

@login_required
def found_user(request, usearch):
    searchword = usearch
    users = User.objects.filter(username__contains=usearch)
    return render(request, 'webvinyl/found_user.html', {'users':users, 'searchword':searchword})

@login_required
def search_music(request):
    if request.method == "POST":
        form = MusicSearchForm(request.POST)
        if form.is_valid():
            return redirect('found_music', change=1)  
    else:
        form = MusicSearchForm()    
    return render(request, 'webvinyl/search_music.html', {'form': form})     

@login_required
def adv_search_music(request): 
    if request.method == "POST":
        form = AdvancedSearchForm(request.POST)
        if form.is_valid():
            return redirect('found_music', change=2)  
    else:
        form = AdvancedSearchForm()  
    return render(request, 'webvinyl/adv_search_music.html', {'form': form}) 


@login_required
def found_music(request, change):
    if request.method == "POST":
        if change==2:
            artist = request.POST.get("artist")
            catno = request.POST.get("number")
            label = request.POST.get("label")
            title = request.POST.get("name")
            vinyls = advanced_search(artist, title, catno, label)
            searchwords = []
            if artist: searchwords.append("Künstler: " + artist)
            if title: searchwords.append("Titel: " + title)
            if label: searchwords.append("Label: " + label)
            if catno: searchwords.append("Katalognummer: " + catno)
            searchword = ", ".join(str(x) for x in searchwords)
        else:
            searchword = request.POST.get("MusicSearchWord")
            vinyls = basic_search(searchword)
    else:    
        form = MusicSearchForm()
        
    return render(request, 'webvinyl/found_music.html', {"vinyls":vinyls, "searchword":searchword,})    
 

@login_required
def vinyl_detail(request, masterid):
    
    vinyl = vinyl_search(masterid) 
    in_collection = request.user.profile.albums.filter(pk=vinyl["id"]).exists()
    try:
        spotify_uri = get_spotify_uri(vinyl["title"], vinyl["artist"])
    except:
        spotify_uri = ""
    return render(request, 'webvinyl/vinyl_detail.html', {'vinyl':vinyl, 'id':masterid, 'spotify_uri':spotify_uri, 'in_collection':in_collection})    

@login_required
def follower(request, pk):
    followed_user = get_object_or_404(User, pk=pk)
    followed = followed_user.profile.followed_by.all()
    follower_count = len(followed_user.profile.followed_by.all())
    return render(request, 'webvinyl/follower_list.html', {'followed': followed, 'follower_count': follower_count, 'followed_user': followed_user})

@login_required
def following(request):
    follows = request.user.profile.follows.all()
    return render(request, 'webvinyl/following.html',{'follows':follows})    

@login_required
def add_album(request, albumkey):
    try:
        to_add = Album.objects.get(pk=albumkey)
    except:
        vinyl = vinyl_search(albumkey)
        to_add = Album(vinyl["id"], vinyl["artist"], vinyl["title"], vinyl["cover_art"])
        to_add.save()
    user = request.user
    if to_add in user.profile.albums.all():
        user.profile.albums.remove(to_add)
    else:
        user.profile.albums.add(to_add)
        s ="hat das Album "+to_add.title+" von "+to_add.artist +" hinzugefügt."
        post = Post(content=s, author=request.user.profile)
        post.save()
    return redirect('vinyl_detail', masterid=to_add.pk)

@login_required
def like(request, postkey):
    to_like = get_object_or_404(Post, pk=postkey)
    user = request.user
    if user.profile in to_like.favourite.all():
        to_like.favourite.remove(user.profile)
    else:
        to_like.favourite.add(user.profile)
    next = request.POST.get('next', '/')
    return HttpResponseRedirect(next)

@login_required
def profile(request, pk):
    follows = request.user.profile.follows.all()
    user = request.user
    if user.pk != pk:
        user = get_object_or_404(User, pk=pk)
  
    follower_count = len(user.profile.followed_by.all())
    follow_count = len(request.user.profile.follows.all())
    collection = user.profile.albums.all()
    return render(request, 'webvinyl/profile.html', {'profile_user': user, 'profile_data': user.profile, 'follows': follows, 'follower_count': follower_count, 'follow_count': follow_count, 'collection': collection,})
        
@login_required
def follow(request, profilekey):
    to_follow = get_object_or_404(User, pk=profilekey)
    user = request.user
    if user.profile in to_follow.profile.followed_by.all():
        user.profile.follows.remove(to_follow.profile)
    else:
        user.profile.follows.add(to_follow.profile)
    return redirect('profile', pk=to_follow.pk)

@login_required
def update(request):
    user = request.user
    if request.method == 'POST':
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=user.profile)
        if p_form.is_valid():
            p_form.save()
            messages.success(request, f'Dein Account wurde geändert.')
            return redirect('profile', pk=user.pk)
    else:
        p_form = ProfileUpdateForm(instance=user.profile)

    context ={
        'p_form':p_form,
        'profile_user':user
    }
    return render(request, 'webvinyl/update.html', context)
