""" In dieser Datei werden die Klassen definiert, aus denen die Datenbank-Tabellen für die "wenvinyl"-App generiert werden """

from django.db import models
from django.utils import timezone
from users.models import Profile
from django.urls import reverse

#Klasse für die Posts der Nutzer
# favourite ermöglicht beliebig vielen Nutzern beliebig viele Posts zu 'liken'
class Post(models.Model):
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='posts')
    favourite = models.ManyToManyField(Profile, related_name='favourited_by', blank=True)

    def __str__(self):
        return f'{self.pk} {self.date_posted} {self.author.user.username}'

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})
    

#Klasse für die Kommentare auf Posts
class Comment(models.Model):
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='comments')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')

    def __str__(self):
        return f'{self.date_posted} {self.author.user.username}, comment on {self.post.pk}'

#Klasse für die lokal gespeicherten Albeninformationen
# owner ermöglicht beliebig vielen Nutzern beliebig viele Alben zu besitzen
class Album(models.Model):
    identifier = models.CharField(max_length=20, primary_key=True)
    artist = models.CharField(max_length=50, blank=True)
    title = models.CharField(max_length=50, blank=True)
    cover_uri = models.URLField(blank=True)
    owner = models.ManyToManyField(Profile, related_name='albums', blank=True)


    def __str__(self):
        return self.identifier