"""
In dieser Datei wird festgelegt welcher View geladen wird, wenn eine bestimmte URL angesprochen wird
"""

from django.urls import path
from . import views
from .views import  PostUpdateView, PostDeleteView
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.base, name='base'),
    path('profile/<int:pk>/', views.profile, name='profile'),
    path('search_user/', views.search_user, name='search_user'),
    path('found_user/<str:usearch>/', views.found_user, name='found_user'),
    path('search_music/', views.search_music, name='search_music'),
    path('found_music/<int:change>', views.found_music, name='found_music'),
    path('vinyl/<int:masterid>/', views.vinyl_detail, name='vinyl_detail'),
    path('follower/<int:pk>/', views.follower, name='follower'),
    path('following/', views.following, name='following'),
    path('post/<int:pk>/', views.post_detail, name='post-detail'),
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
    path('follow/<int:profilekey>/', views.follow, name='follow-user'),
    path('add_vinyl/<int:albumkey>/', views.add_album, name='add-album'),
    path('update/', views.update, name="user-update" ),
    path('like/<int:postkey>', views.like, name="like-post" ),
    path('adv_search/', views.adv_search_music, name='adv_search_music')
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)