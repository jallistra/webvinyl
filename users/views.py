"""
In dieser Datei werden die Logiken definiert, die beim Aufruf einer URL der users-App ausgeführt werden.
"""

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth.decorators import login_required
from .models import Profile, User

#Diese Methode beschreibt die Verarbeitung der Eingaben auf der Registrierungsseite. Genutzt wird dabei ein vorher definiertes Formular
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, f'Dein Account wurde erstellt. Du kannst dich jetzt anmelden.')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})

