"""
In dieser Datei werden die Model-Klassen der users-App angegeben, 
deren zugeordnete Datenbank-Tabellen über das Admin-Menü verwaltet werden können sollen.
"""

from django.contrib import admin
from .models import Profile

admin.site.register(Profile)

