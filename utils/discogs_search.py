"""Diese Datei enthält die Discogs API Suchfunktionen"""

import json, requests

#Führt eine einfache Suche durch, die auf der search_music Seite benötigt wird
# es wird eine generelle Suchanfrage an die discogs API gestellt, die einen Suchbegriff enthält, 
# und eine Liste der Ergebnisse zurückliefert
def basic_search(searchword):

    payload = {'q': searchword, 'type': "master", 'token': 'lCRrtFAXEkwVCUizIpOrAkAsZFcyFRVGocwAlzdA'}
    query = requests.get('https://api.discogs.com/database/search', params = payload)

    results = json.loads(query.text)["results"]

    search_results = []

    for r in results:

        if "year" in r:
            year = r["year"]
        else:
            year = ""

        search_results.append({
            "cover_art":r["cover_image"],
            "title":r["title"].split("- ")[1],
            "artist":r["title"].split(" -")[0],
            "label":r["label"][0],
            "year":year,
            "id":r["id"]
        })
   
    return search_results

#Führt eine erweitete Suche durch, die auf der adv_search_music Seite benötigt wird 
# es wird eine detaillierte Suchanfrage an die discogs API gestellt, die Suchbegriffe für spezifische Attribute enthält, 
# und eine Liste der Ergebnisse zurückliefert
def advanced_search(artist, title, catno, label):
    payload = {'type': 'master', 'token': 'lCRrtFAXEkwVCUizIpOrAkAsZFcyFRVGocwAlzdA'}
    if artist:
        payload['artist'] = artist
    if title:
        payload['release_title'] = title
    if catno:
        payload['catno'] = catno
    if label:
        payload['label'] = label

    query = requests.get('https://api.discogs.com/database/search', params = payload)

    results = json.loads(query.text)["results"]

    search_results = []

    for r in results:

        if "year" in r:
            year = r["year"]
        else:
            year = ""

        search_results.append({
            "cover_art":r["cover_image"],
            "title":r["title"].split("- ")[1],
            "artist":r["title"].split(" -")[0],
            "label":r["label"][0],
            "year":year,
            "catno":r["catno"],
            "id":r["id"]
        })
   
    return search_results

#Ruft Album-Info von der Discogs API ab
# es werden die discogs-ID des Albums als Parameter übergeben und die Infos zu dem betreffenden Album abgerufen
def vinyl_search(masterid):

    url = f'https://api.discogs.com/masters/{masterid}?token=lCRrtFAXEkwVCUizIpOrAkAsZFcyFRVGocwAlzdA'
    query = requests.get(url)

    r = json.loads(query.text)

    if "year" in r:
        year = r["year"]
    else:
        year = ""

    tracklist = []

    for track in r["tracklist"]:
        tracklist.append({
            "number":track["position"],
            "title":track["title"],
            "duration":track["duration"]
        })

    search_result = {
        "cover_art":r["images"][0]["resource_url"],
        "title":r["title"],
        "artist":r["artists"][0]["name"],
        "year":year,
        "tracklist":tracklist,
        "genre":r["genres"][0],
        "id":r["id"]
    }

    return search_result

